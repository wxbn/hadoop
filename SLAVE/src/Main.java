import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Main {

    static void SxToUMx(String path) throws IOException {
        Map<String, Integer> map = new HashMap<String, Integer>();

        String inputFilePath = path;
        BufferedReader br = new BufferedReader(new FileReader(inputFilePath));

        Character index = path.charAt(path.lastIndexOf('.') - 1);
        String outputFilePath = "/tmp/hadoop/maps/UM" + index + ".txt";
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));

        String line;
        while ((line = br.readLine()) != null) {
            for (String word : line.split(" ")) {
                writer.write(word + " " + 1);
                writer.newLine();

                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                } else {
                    map.put(word, 1);
                }
            }
        }
        writer.close();

        for (Map.Entry<String, Integer> entry : map.entrySet())  {
            String word = entry.getKey();
            System.out.println(word);
        }
    }

    static void UMxToSMx(String path, String word, BufferedWriter writer) throws IOException {
        String inputFilePath = path;
        BufferedReader br = new BufferedReader(new FileReader(inputFilePath));
        String line;
        while ((line = br.readLine()) != null) {
            String[] data = line.split(" ");
            String w = data[0];
            if (w.equals(word)) {
                writer.write(line);
                writer.newLine();
            }
        }
    }

    public static void reduce(BufferedReader br, BufferedWriter writer) throws IOException {
        Map<String, Integer> map = new HashMap<String, Integer>();

        String line;
        while ((line = br.readLine()) != null) {
            String[] data = line.split(" ");
            String word = data[0];
            Integer count = Integer.parseInt(data[1]);
            if (map.containsKey(word)) {
                map.put(word, map.get(word) + count);
            } else {
                map.put(word, count);
            }
        }

        for (Map.Entry<String, Integer> entry : map.entrySet())  {
            String w = entry.getKey();
            Integer count = entry.getValue();
            writer.write(w + " " + count);
            writer.newLine();
        }
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        if (Integer.parseInt(args[0]) == 0) {
            SxToUMx(args[1]);
        } else if (Integer.parseInt(args[0]) == 1) {
            String word = args[1];
            String outputFilePath = args[2];
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));
            for (int i = 3; i < args.length; i++) {
                UMxToSMx(args[i], word, writer);
            }
            writer.close();
        } else {
            String word = args[1];

            String inputFilePath = args[2];
            BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));

            String outputFilePath = args[3];
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));

            reduce(reader, writer);
        }
    }
}