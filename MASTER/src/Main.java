import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

class ThreadedProcess extends Thread {
    Map<String, Integer> map = new HashMap<String, Integer>();
    Integer machine_index;
    String addr;
    Integer i;

    public ThreadedProcess(Integer machine_index, String addr, Integer i) {
        this.machine_index = machine_index;
        this.addr = addr;
        this.i = i;
    }

    void process() throws IOException, InterruptedException {
        ProcessBuilder pb1 = new ProcessBuilder("ssh", addr, "mkdir", "-p", "/tmp/hadoop/splits", ";", "mkdir", "-p", "/tmp/hadoop/maps", ";", "mkdir", "-p", "/tmp/hadoop/reduces", "exit");
        Process p1 = pb1.start();
        p1.waitFor(5, TimeUnit.SECONDS);

        ProcessBuilder pb2 = new ProcessBuilder("scp", "-r", "/tmp/hadoop/splits", addr + ":/tmp/hadoop");
        Process p2 = pb2.start();
        p2.waitFor(5, TimeUnit.SECONDS);

        ProcessBuilder pb = new ProcessBuilder("ssh", "-T", addr, "java", "-jar", "/tmp/hadoop/SLAVE.jar", "0", i.toString(), ";", "exit");
        Process p = pb.start();
        p.waitFor(15, TimeUnit.SECONDS);

        if (!p.isAlive()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            String result = builder.toString();

            BufferedReader reader2 = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder2 = new StringBuilder();
            String line2 = null;
            while ((line2 = reader2.readLine()) != null) {
                builder2.append(line2);
                builder2.append(System.getProperty("line.separator"));
            }
            String result2 = builder2.toString();

            if (p1.exitValue() == 0 && p.exitValue() == 0) {
                System.out.println("UM" + i + " - " + addr);
                //System.out.println(result);
                //System.out.println(result2);

                for (String word : result2.split("\\r?\\n")) {
                    map.put(word, i);
                }
            } else {
                System.out.println(addr + ": Error");
            }
        }

    }

    public void run() {
        try {
            process();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {
        Map<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();

        BufferedReader br = new BufferedReader(new FileReader("deploy.txt"));
        Vector<String> machines = new Vector<>();
        String addr;
        while ((addr = br.readLine()) != null) {
            machines.add(addr);
        }

        int nb_of_Sx = new File("/tmp/hadoop/splits").listFiles().length;

        ArrayList<ThreadedProcess> procs = new ArrayList<ThreadedProcess>();
        for (Integer i = 0; i < nb_of_Sx; i++) {
            Integer machine_index = i % machines.size();
            addr = machines.get(machine_index);

            procs.add(new ThreadedProcess(machine_index, addr, i));
            procs.get(procs.size() - 1).start();
        }

        for (ThreadedProcess proc : procs) {
            proc.join();
            for (Map.Entry<String, Integer> entry : proc.map.entrySet())  {
                String word = entry.getKey();
                Integer machine_nb = entry.getValue();

                if (map.containsKey(word)) {
                    ArrayList<Integer> machines_list = map.get(word);
                    machines_list.add(machine_nb);
                    map.put(word, machines_list);
                } else {
                    ArrayList<Integer> machines_list = new ArrayList<Integer>();
                    machines_list.add(machine_nb);
                    map.put(word, machines_list);
                }
            }
        }

        System.out.println("phase de MAP terminée");

        for (Map.Entry<String, ArrayList<Integer>> entry : map.entrySet())  {
            String word = entry.getKey();
            ArrayList<Integer> counts = entry.getValue();
            System.out.print(word + " < ");
            for (Integer c : counts) {
                System.out.print("UM" + c + " ");
            }
            System.out.println(">");
        }

        List<String> keyList = new ArrayList<String>(map.keySet());
        for(Integer i = 0; i < keyList.size(); i++) {
            Integer machine_index = i % machines.size();
            addr = machines.get(machine_index);
            String word = keyList.get(i);
            ArrayList<Integer> counts = map.get(word);

            for (Integer c : counts) {
                String sourceMachine = machines.get(c % machines.size());

                if (!addr.equals(sourceMachine)) {
                    ProcessBuilder pb = new ProcessBuilder("scp", "-r", sourceMachine + ":/tmp/hadoop/maps/UM" + c + ".txt", addr + ":/tmp/hadoop/maps");
                    Process p = pb.start();
                    p.waitFor(5, TimeUnit.SECONDS);
                }
            }

            ArrayList<String> commands = new ArrayList<>(List.of("ssh", "-T", addr, "java", "-jar", "/tmp/hadoop/SLAVE.jar", "1", word, i.toString()));
            for (Integer c : counts) {
                commands.add(c.toString());
            }
            ProcessBuilder pb = new ProcessBuilder(commands);
            Process p = pb.start();
            p.waitFor(5, TimeUnit.SECONDS);

            commands = new ArrayList<>(List.of("ssh", "-T", addr, "java", "-jar", "/tmp/hadoop/SLAVE.jar", "2", word, i.toString()));
            for (Integer c : counts) {
                commands.add(c.toString());
            }
            ProcessBuilder pb2 = new ProcessBuilder(commands);
            Process p2 = pb2.start();
            p2.waitFor(5, TimeUnit.SECONDS);
        }

    }
}